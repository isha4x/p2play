P2Play es una aplicacion android para Peertube.

¿Que es Peertube? https://github.com/Chocobozzz/PeerTube/

Caracteristicas
* Show recent, popular and local list of videos.
* Reproduce videos
* Login and register in your instance
* Show uploaded videos
* Subscribe to accounts
* Show your subscription videos
* Show your history
* Rate videos
* Show and make commentaries
* Splash screen
* Search videos
* Infinite scroll
* Share videos
* Report videos
* Peertube profiles
